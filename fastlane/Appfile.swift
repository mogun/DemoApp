var appIdentifier: String { return "[[APP_IDENTIFIER]]" } // The bundle identifier of your app
var appleID: String { return "[[APPLE_ID]]" } // Your Apple email address

//var appIdentifier: String { return "com.softence.DemoApp" } // The bundle identifier of your app
//var appleID: String { return "iosapp@appudvikleren.dk" } // Your Apple email address
//
//var teamID: String { return "5PN99X4SRN" } // Developer Portal Team ID
//var itcTeam: String? { return "39941800" } // App Store Connect Team ID (may be nil if no team)



// For more information about the Appfile, see:
//     https://docs.fastlane.tools/advanced/#appfile
